from .testcase import TestCase
import texplotlibx as tpl


class TestFigureFramework(TestCase):
  def is_figure(self, obj):
    self.assertIsInstance(obj, tpl.figure)

  def test_figure_creation(self):
    f = tpl.figure()
    self.is_figure(f)

  def test_two_figure_creation(self):
    f1 = tpl.figure()
    f2 = tpl.figure()
    self.is_figure(f1)
    self.is_figure(f2)

  def test_many_figure_creation(self):
    N = 1000
    f = [tpl.figure() for _ in range(N)]
    arg = zip([self] * N, f)
    map(self.__class__.is_figure, arg)
