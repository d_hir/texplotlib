import unittest
import time


class TestCase(unittest.TestCase):
  def setUp(self):
    self.toleratedTime = 0.1
    self.startTime = time.time()

  def tearDown(self):
    self.endTime = time.time()

  def __del__(self):
    dt = self.endTime - self.startTime
    dtstr = "%g s" % dt
    if dt > self.toleratedTime:
      dtstr = "\033[;1m%s\033[0m" % dtstr
    print("\tTest took %s" % dtstr)


if __name__ == "__main__":
  unittest.main()
