from .testcase import TestCase
import texplotlibx as tpl


class TestPlot(TestCase):
  def test_typical(self):
    f = tpl.figure()
    x = list(range(-5, 5))
    y = [xi * xi for xi in x]
    f.plot(x, y)

  def test_single(self):
    f = tpl.figure()
    x = [3.141592653]
    y = [2.718281828]
    f.plot(x, y)

  def test_large(self):
    f = tpl.figure()
    N = int(1e5)
    x = [k / N for k in range(N + 1)]
    y = [xi ** xi for xi in x]
    f.plot(x, y)
