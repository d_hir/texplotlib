SHELL=/usr/bin/env bash

define pipinstall
	@echo "Installing or updating python package \"$(1)\""
	@python -m pip install --upgrade $(1) | grep --invert-match ^Requirement; true
endef

PYPACKAGES=twine build
PYPACKTRGT=$(patsubst %, py_%, $(PYPACKAGES))

MAJOR:=$(shell awk -F'[\"\.]' '/^[[:blank:]]*version/ {print $$2}' setup.py)
MINOR:=$(shell awk -F'[\"\.]' '/^[[:blank:]]*version/ {print $$3}' setup.py)
PATCH:=$(shell awk -F'[\"\.]' '/^[[:blank:]]*version/ {print $$4}' setup.py)

NEXTMAJOR:=$(MAJOR)
NEXTMINOR:=$(MINOR)
NEXTPATCH:=$(PATCH)

ifeq ($(REV), major)
NEXTMAJOR:=$(shell echo $$(( $(MAJOR) + 1 )))
NEXTMINOR:=0
NEXTPATCH:=0
endif
ifeq ($(REV), minor)
NEXTMINOR:=$(shell echo $$(( $(MINOR) + 1 )))
NEXTPATCH:=0
endif
ifeq ($(REV), patch)
NEXTPATCH:=$(shell echo $$(( $(PATCH) + 1 )))
endif

.PHONY: revision build check publish $(PYPACKTRGT) test py_pip

revision:
	@if [[ "$(MAJOR)" -eq "$(NEXTMAJOR)" && "$(MINOR)" -eq "$(NEXTMINOR)" && "$(PATCH)" -eq "$(NEXTPATCH)" ]]; then \
	echo Wrong or no revision specified \(REV=\"$(REV)\"\), allowed are \"major\", \"minor\", \"patch\" \
	&& false; fi
	@if git status | grep -q 'setup.py'; then \
	echo setup.py has been modified. Cannot create clean version update commit. Please commit or remove changes. \
	&& false; fi
	@sed -i 's/version=\"$(MAJOR).$(MINOR).$(PATCH)\"/version=\"$(NEXTMAJOR).$(NEXTMINOR).$(NEXTPATCH)\"/g' setup.py
	@git reset > /dev/null
	@git add setup.py
	@git commit -m "(auto-versioning: $(REV)) Update to $(NEXTMAJOR).$(NEXTMINOR).$(NEXTPATCH)"
	@git tag v$(NEXTMAJOR).$(NEXTMINOR).$(NEXTPATCH)

py_pip:
	$(call pipinstall,pip)

$(PYPACKTRGT): py_pip
	$(call pipinstall,$(patsubst py_%,%,$@))

build dist: py_build
	@echo "Removing previous build remnants" && rm dist -rf
	@python -m build

check: dist py_twine
	@python -m twine check dist/* | tee >(grep --quiet --ignore-case --extended-regexp "(fail|warning|error)" && echo "twine check found errors/warnings!" || true)

publish: check py_twine
	@echo "Publishing package"
	@python -m twine upload dist/*

test:
	@python -m unittest discover -v

clean:
	@rm dist -rf

install: py_pip check
	$(call pipinstall,.)
